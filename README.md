# What's this library?

This library it's a lightweight implementation of the BCode Encoder/Decoder used by torrent protocol. Which represent
different kinds of data in the followings ways:

### Strings

#### 5:Hello → Hello

- 5 is the length of the String
- : is used a separator
- Hello is the string

### Integers

#### i3e → 3

- i Is the beginning delimiter
- 3 Is the representation of the integer in base ten ASCII
- e Is the ending delimiter

### Lists

#### l5:hello5:helloe →  ['hello','hello']

- l Is the beginning delimiter
- 5:hello5:hello are the Bencoded values
- e Is the ending delimiter

### Dictionaries

#### d3:cow3:moo4:spam4:eggse → { "cow":"moo", "spam":"eggs" }

- d Is the beginning delimiter
- 3:cow3:moo4:spam4:eggs are the Bencoded keys/values
- e Is the ending delimiter

More details can be found at the [wiki](https://wiki.theory.org/BitTorrentSpecification)