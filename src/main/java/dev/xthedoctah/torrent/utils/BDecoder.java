package dev.xthedoctah.torrent.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class BDecoder {
    private final Pattern numberPattern = Pattern.compile("-?\\d+(\\.\\d+)?");
    private final Pattern integerBCodePattern = Pattern.compile("[i][\\d]*[e]");
    private final BEncoder bEncoder = new BEncoder();

    public Object decodeMessage(String message) {
        String delimiter = message.substring(0, 1);
        if (isNumeric(delimiter)) {
            return parseString(message);
        } else if (delimiter.equalsIgnoreCase("i") && integerBCodePattern.matcher(message).matches()) {
            return parseInt(integerBCodePattern.matcher(message).group());
        } else if (delimiter.equalsIgnoreCase("l")) {
            return parseList(message);
        } else if (delimiter.equalsIgnoreCase("d")) {
            return parseObject(message);
        }
        throw new RuntimeException("Not a valid object" + message);
    }


    public String parseString(String message) {
        String length = message.split(":")[0];
        int stringLength = Integer.parseInt(length);
        return message.substring(length.length() + 1, stringLength + length.length() + 1);
    }

    public Integer parseInt(String message) {
        return Integer.parseInt(message.replaceAll(Pattern.compile("[A-z]").pattern(), ""));
    }

    public List<Object> parseList(String message) {
        String contenuto = message.substring(1, message.length() - 1);
        ArrayList<Object> content = new ArrayList<>();
        while (!contenuto.equals("") && !contenuto.startsWith("e")) {
            Object result = this.decodeMessage(contenuto);
            String toRemove = Pattern.quote(bEncoder.encodeMessage(result));
            contenuto = contenuto.replaceFirst(toRemove, "");
            content.add(result);
        }
        return content;
    }

    public HashMap<String, Object> parseObject(String message) {
        String contenuto = message.substring(1, message.length() - 1);
        HashMap<String, Object> content = new HashMap<>();
        while (!contenuto.equals("") && !contenuto.startsWith("e")) {
            String length = contenuto.split(":")[0];
            String key = contenuto.split(":")[1];
            key = key.substring(0, parseInt(length));
            contenuto = contenuto.replaceFirst(Pattern.quote(bEncoder.encodeMessage(key)), "");
            Object result = this.decodeMessage(contenuto);
            String toRemove = Pattern.quote(bEncoder.encodeMessage(result));
            contenuto = contenuto.replaceFirst(toRemove, "");
            content.put(key, result);
        }
        return content;
    }

    private boolean isNumeric(String value) {
        return numberPattern.matcher(value).matches();
    }
}
