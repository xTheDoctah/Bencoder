package dev.xthedoctah.torrent.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BEncoder {

    public String encodeMessage(Object object) {
        try {
            if (object instanceof String) {
                return this.encode(String.valueOf(object));
            } else if (object instanceof Number) {
                return this.encode((Number) object);
            } else if (object instanceof Collection) {
                return this.encode((Collection<Object>) object);
            } else if (object instanceof Map) {
                return encode((Map) object);
            }
            return encode(object);
        } catch (Exception ex) {
            throw new RuntimeException("Errore durante la conversione", ex);
        }
    }

    public String encode(String string) {
        return String.format("%d:%s", string.length(), string);
    }

    public String encode(Number number) {
        return String.format("i%de", number);
    }

    public String encode(Map<String, Object> map) {
        return "d".concat(map.keySet().stream().map(key -> encodeMessage(key).concat(encodeMessage(map.get(key)))).collect(Collectors.joining())).concat("e");
    }

    public String encode(Collection<Object> list) {
        return "l".concat(list.stream().map(this::encodeMessage).collect(Collectors.joining())).concat("e");
    }

    public String encode(Object dictionary) throws ClassNotFoundException {
        return "d" + Arrays.stream(Class.forName(dictionary.getClass().getName()).getDeclaredFields()).map(field -> {
            field.setAccessible(true);
            try {
                return this.encode(field.getName()).concat(this.encodeMessage(field.get(dictionary)));
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.joining()) + "e";
    }

}