package dev.xthedoctah.torrent.utils;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //BDecoder.main(new String[]{});
        String enc = new BEncoder().encodeMessage(new Dto());
        System.out.println(enc);
        enc = new BEncoder().encodeMessage(new BDecoder().decodeMessage(enc));
        System.out.println(enc);
    }

    public static class Dto {
        String y = "Ciao";
        String dg = "dadsa";
        String v = "dasdas";
        Inner2 inner2 = new Inner2();
        List lista = Collections.singletonList("ciao");
    }

    public static class Inner2 {
        String x = "porco";
    }
}

